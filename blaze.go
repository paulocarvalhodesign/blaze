package blaze

import (
	//"bitbucket.org/paulocarvalhodesign/blaze/applications/backend/router"
	"bitbucket.org/paulocarvalhodesign/blaze/config"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xweb/log"
	"net/http"
	//"time"
)

var Conf config.Config

func init() {

}

func SetConf(conf config.Config) {
	Conf = conf
}

func Run(srv *http.Server) {
	orm.Debug = false
	for _, db := range Conf.Databases {
		orm.RegisterDataBase(
			db.Connection,
			db.Type,
			db.User+":"+db.Password+"@tcp("+db.Server+":"+db.Port+")/"+db.Name+"?charset=utf8",
			db.MaxConnections)
	}
	//mux := router.GetAppRoutes()
	//back := http.Server{
	//	Handler:      mux,
	//	Addr:         "localhost:8088",
	//	WriteTimeout: 10 * time.Second,
	//	ReadTimeout:  10 * time.Second,
	//}
	//go back.ListenAndServe()
	log.Fatal(srv.ListenAndServe())
}

func GetConfig() config.Config {
	return Conf
}
