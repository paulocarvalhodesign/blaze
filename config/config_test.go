package config

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestReadConfig(t *testing.T) {
	var conf = Config{}
	conf = ReadConfig("/Users/Paulo/GoWorkspace/src/phintel/conf/testing.toml")
	assert.Equal(t, Config{Title: "testing"}, conf)
}
