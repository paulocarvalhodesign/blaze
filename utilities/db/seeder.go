package seeder

import (
	"fmt"
	"path/filepath"
	"os"
	"io/ioutil"
	"os/exec"
	"strings"
)

var Seeders = []SeederInterface{}

type SeederInterface interface {
	Seed() error
	GetName() string
}

type Seeder struct {
	//implements...
	SeederInterface

	//fields
	Name string
}

func (s *Seeder) Seed() error{
	return fmt.Errorf("Please implement Seed() for your seeder struct")
}

func (s *Seeder) GetName() string{
	return s.Name
}

func Register(s SeederInterface){
	Seeders = append(Seeders, s)
}

// Run the seeders
func Run(path string) {

	currpath, _ := os.Getwd()
	seedersPath := filepath.Join(currpath, "database", "seeders")
	_, err := os.Stat(seedersPath)
	if err != nil {
		fmt.Println("seeders directory not found. Expected to find it at " + seedersPath)
		return
	}

	goFile := "s.go"
	goBin := "s"
	seederLauncherFile := filepath.Join(seedersPath, goFile)

	//copy seeder template to location
	file, err := ioutil.ReadFile("vendor/"+path+"/seeder.tpl")
	if err != nil{
		fmt.Println(err.Error())
	}
	ioutil.WriteFile(seederLauncherFile, file, os.ModePerm)

	//compile seeder launch script
	os.Chdir(seedersPath)
	cmd := exec.Command("go", "build", "-o", goBin)
	if _, err := cmd.CombinedOutput(); err != nil {
		fmt.Println("Could not build seeder binary: %s\n", err)
		os.Exit(2)
	}


	//run the seeder
	cmd = exec.Command("./" + goBin)
	out, err := cmd.CombinedOutput();
	if err != nil {
		fmt.Println("[ERRO] Could not run seeder binary: %s\n", err)
		os.Exit(2)
	}
	printOutput(string(out))


	removeTempFile(seedersPath, goFile)
	removeTempFile(seedersPath, goBin)
}

func removeTempFile(dir, file string) {
	os.Chdir(dir)
	if err := os.Remove(file); err != nil {
		fmt.Println("[WARN] Could not remove temporary file: %s\n", err)
	}
}


// formatShellOutput formats the normal shell output
func printOutput(o string) {
	for _, line := range strings.Split(o, "\n") {
		if line != "" {
			fmt.Println(line)
		}
	}
}
