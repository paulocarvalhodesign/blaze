package slice

// Check if the array or slice contains a value.
func Int64Contains(arr []int64, v int64) bool {

	// Loop the array/slice items.
	for _, item := range arr {

		// If the item exists, its true.
		if item == v {
			return true
		}
	}

	// Item not found.
	return false
}

// Replicate the above method as needed for ints etc...
