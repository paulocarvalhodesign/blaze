**Run Migrations**

`bee migrate -driver='mysql' -conn='root:password@tcp(127.0.0.1:3306)/database'`

**Build and Watch**

_Install tool_

`go get github.com/pilu/fresh`

_then just run and will rebuild and watch changes_

`fresh`
