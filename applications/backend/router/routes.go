package router

import (
	"bitbucket.org/paulocarvalhodesign/blaze/applications/backend/handlers/base"
	"bitbucket.org/paulocarvalhodesign/blaze/router"
)

func GetAppRoutes() *router.Mux {
	r := router.New()
	r.SubRoute("/admin", Base.GetRoutes())
	return r
}
