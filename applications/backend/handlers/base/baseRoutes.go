package Base

import (
	"bitbucket.org/paulocarvalhodesign/blaze/middleware"
	"bitbucket.org/paulocarvalhodesign/blaze/middleware/logger"
	"bitbucket.org/paulocarvalhodesign/blaze/router"
)

func GetRoutes() *router.Mux {
	r := router.New()
	c := middleware.New(logger.LoggerMiddleware{})
	r.Get("/login", c.Use(Index))
	return r
}
