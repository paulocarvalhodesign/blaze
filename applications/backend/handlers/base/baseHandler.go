package Base

import (
	"bitbucket.org/paulocarvalhodesign/blaze/utilities/statistics"
	"github.com/matryer/respond"
	"net/http"
)

func Index(w http.ResponseWriter, r *http.Request) {
	data := statistics.StatisticsMap.GetMap()
	respond.With(w, r, http.StatusOK, data)
}
