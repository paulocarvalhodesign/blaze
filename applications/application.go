package application

import "bitbucket.org/paulocarvalhodesign/blaze/router"

type AppInterface interface {
	GetRoutes() *router.Mux
	GetPath() string
}

func GetAppRoutes(app AppInterface) *router.Mux {
	return app.GetRoutes()
}

func GetPath(app AppInterface) string {
	return app.GetPath()
}
