package auth

import (
	"bitbucket.org/paulocarvalhodesign/blaze"
	"bitbucket.org/paulocarvalhodesign/blaze/crypt"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"net/http"
	"time"
)

type CustomClaims struct {
	Data string `json:"Data"`
	jwt.StandardClaims
}

func Middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		conf := blaze.GetConfig()
		token := r.Header.Get("X-token")
		cls := CustomClaims{}
		parsedToken, err := jwt.ParseWithClaims(token, &cls, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method %v", token.Header["alg"])
			}
			return []byte(conf.Jwt.JwtKey), nil
		})
		if err != nil {
			w.WriteHeader(401)
			w.Write([]byte(err.Error()))
			return
		}
		if !parsedToken.Valid {
			w.WriteHeader(401)
			w.Write([]byte("Invalid Token"))
			return
		}
		parsedData := Crypt.Decrypt([]byte(conf.Jwt.JwtPayloadKey), cls.Data)
		if cls.ExpiresAt < time.Now().Unix() {
			w.WriteHeader(401)
			w.Write([]byte("Expired Token"))
			return
		}
		context.Set(r, "authedUser", parsedData)
		h.ServeHTTP(w, r)
	})
}
