package orm

import (
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/context"
	"net/http"
)

func Middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		Orm := orm.NewOrm()
		context.Set(r, "orm", Orm)
		h.ServeHTTP(w, r)
	})
}
