package bolt

import (
	"github.com/boltdb/bolt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xweb/log"
	"github.com/gorilla/context"
	"net/http"
)

func Middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		db, err := bolt.Open("cache/bolt.db", 0644, nil)
		if err != nil {
			log.Fatal(err)
		}
		defer db.Close()
		context.Set(r, "bolt", db)
		h.ServeHTTP(w, r)
	})
}
