package mongo

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/context"
	"gopkg.in/mgo.v2"
	"net/http"
)

func Middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		mongo, _ := mgo.Dial("localhost")
		defer mongo.Close()
		context.Set(r, "mongo", mongo)
		h.ServeHTTP(w, r)
	})
}
