package mysql

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/context"
	"net/http"
)

type MysqlMiddleware struct{}

func Middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		db, _ := sql.Open("mysql", "root:password@tcp(127.0.0.1:3306)/blaze")
		context.Set(r, "dbMySql", db)
		h.ServeHTTP(w, r)
	})
}
